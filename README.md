# Customer Direct's Javascript Styleguide

_Customer Direct's mostly reasonable approach to writing Javascript_

This project is Customer Direct's Javascript styleguide rules and resources. Instead of reinventing the wheel, this project references excellent documentation on how to write clean, performant, and maintainable javascript.

## Table of Contents

##
[TOC]
##

## Installation

Install this config package and ESLint:

1. Run `npm install eslint-config-cd-base --save-dev`
2. Add `'extends': 'cd-base'` to your .eslintrc

## Meet NVM

Like RVM, NVM is a version manager but for Node.js. Make your life easier and use it. For installation and usage, visit [NVM's GitHub](https://github.com/creationix/nvm) page.

## Resources

### Learning ES6

* [ES6-Learning](https://github.com/ericdouglas/ES6-Learning)
* [ES6 Compatibility Table](https://kangax.github.io/compat-table/es6/)
* [Comprehensive Overview of ES6 Features](http://es6-features.org/#Constants)

### Linters

* [ESlint](http://eslint.org/) - CD style [.eslintrc]

### Other Style Guides

* [AirBnB](https://github.com/airbnb/javascript)
* [Walmart Labs](https://github.com/walmartlabs/eslint-config-defaults)
* [Google JavaScript Style Guide](https://google.github.io/styleguide/javascriptguide.xml)
* [jQuery Core Style Guidelines](http://contribute.jquery.org/style-guide/js/)
* [Principles of Writing Consistent, Idiomatic JavaScript](https://github.com/rwaldron/idiomatic.js)

### Articles / Videos

* [Rethinking Best Practices](https://www.youtube.com/watch?v=x7cQ3mrcKaY)
* [Mixins Are Dead. Long Live Composition](https://medium.com/@dan_abramov/mixins-are-dead-long-live-higher-order-components-94a0d2f9e750#.5l8fty3uo)
* [The Open Minded Explorer’s Guide to Object Composition](https://medium.com/javascript-scene/the-open-minded-explorer-s-guide-to-object-composition-88fe68961bed#.qooqed4ly)


### Books

* [JavaScript: The Good Parts](https://www.amazon.com/JavaScript-Good-Parts-Douglas-Crockford/dp/0596517742/184-6525665-3128047?ie=UTF8&*Version*=1&*entries*=0) - Douglas Crockford

### Podcasts

* [JS Jabber](https://devchat.tv/js-jabber/)

## Maintenance

This document and node package is maintained by the wizards at [Customer Direct, LLC](http://customerdirect.com) in St. Louis, MO.