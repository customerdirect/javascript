# eslint-config-cd-react


## Installation

To install this config package and ESLint:

```
npm install eslint eslint-plugin-react eslint-config-cd-react --save-dev
```

## Usage

Add `'extends': 'cd-react'` to your .eslintrc file.

## Maintenance

This package is maintained by the wizards at [Customer Direct, LLC](http://customerdirect.com) in St. Louis, MO.
