module.exports = {
  rules: {
    // Use double quotes
    'jsx-quotes': [1, 'prefer-double'],

    // Enforce boolean attribute notiation
    'react/jsx-boolean-value': [1, 'never'],

    // Validate JSX prop indentation
    'react/jsx-indent-props': [1, 2],

    // Validate JSX key prop in array or iterator
    'react/jsx-key': 1,

    // Prevent usage of .bind() in JSX props
    'react/jsx-no-bind': [1, {
      ignoreRefs: true,
      allowArrowFunctions: true,
      allowBind: false,
    }],

    // Prevent duplicate props in JSX
    'react/jsx-no-duplicate-props': [1, { ignoreCase: false }],

    // Allow jsx literals
    'react/jsx-no-literals': 0,

    // Disallow undeclared variables in JSX
    'react/jsx-no-undef': 0,

    // Enforce PascalCase for user-defined JSX components
    'react/jsx-pascal-case': [1, {
      allowAllCaps: true,
      ignore: [],
    }],

    // Don't enforce props alphabetical sorting
    'react/jsx-sort-props': 0,

    // Enforce spaces before the closing bracket of self-closing JSX elements
    'react/jsx-space-before-closing': [1, 'always'],

    // enforce spacing around jsx equals signs
    'react/jsx-equals-spacing': [1, 'never'],

    // disallow target="_blank" on links
    'react/jsx-no-target-blank': 0,

    // React must be including in every jsx module
    "react/jsx-uses-react": 1,

    "react/jsx-uses-vars": 1

  }
}
