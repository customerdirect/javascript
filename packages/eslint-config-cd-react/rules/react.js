module.exports = {
  plugins: [ 'react' ],

  rules: {

    // Enforce propTypes declarations alphabetical sorting
    'react/sort-prop-types': [1, {
      ignoreCase: false,
      callbacksLast: false,
    }],

    // Prevent usage of dangerous JSX properties
    'react/no-danger': 0,

    // Prevent usage of deprecated methods
    'react/no-deprecated': [1, { react: '0.15.0' }],

    // Prevent usage of setState in componentDidMount
    'react/no-did-mount-set-state': [1, 'allow-in-func'],

    // Prevent usage of setState in componentDidUpdate
    'react/no-did-update-set-state': [1, 'allow-in-func'],

    // Prevent direct mutation of this.state
    'react/no-direct-mutation-state': 1,

    // Prevent usage of isMounted
    'react/no-is-mounted': 1,

    // Prevent multiple component definition per file
    'react/no-multi-comp': [1, { ignoreStateless: true }],

    // Prevent usage of setState
    'react/no-set-state': 0,

    // Prevent using string references
    'react/no-string-refs': 0,

    // Prevent usage of unknown DOM property
    'react/no-unknown-property': 1,

    // Require ES6 class declarations over React.createClass
    'react/prefer-es6-class': [1, 'never'],

    // Require stateless functions when not using lifecycle methods, setState or ref
    'react/prefer-stateless-function': 1,

    // Prevent missing props validation in a React component definition
    'react/prop-types': [1, { ignore: [], customValidators: [] }],

    // Require render() methods to return something
    'react/require-render-return': 1,

    // Prevent extra closing tags for components without children
    'react/self-closing-comp': 1,

    // Enforce spaces before the closing bracket of self-closing JSX elements
    'react/jsx-space-before-closing': [1, 'always'],

    // Enforce component methods order
    'react/sort-comp': [1, {
      order: [
        'static-methods',
        'lifecycle',
        '/^on.+$/',
        '/^(get|set)(?!(InitialState$|DefaultProps$|ChildContext$)).+$/',
        'everything-else',
        '/^render.+$/',
        'render'
      ],
    }],

    // Prevent missing parentheses around multilines JSX
    'react/wrap-multilines': [1, {
      declaration: true,
      assignment: true,
      return: true
    }],

    // enforce spacing around jsx equals signs
    'react/jsx-equals-spacing': [1, 'never'],

    // disallow target="_blank" on links
    'react/jsx-no-target-blank': 0
  },

  settings: {
    'import/resolver': {
      node: {
        extensions: ['.js', '.jsx', '.json']
      }
    },
    react: {
      pragma: 'React',
      version: '0.15'
    },
  }

}
