module.exports = {
  extends: [
    './rules/jsx.js',
    './rules/react.js'
  ].map(require.resolve),
  rules: {}
}
