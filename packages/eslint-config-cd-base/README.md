# eslint-config-cd-base


## Installation

To install this config package and ESLint:

```
npm install eslint eslint-config-cd-base --save-dev
```

## Usage

Add `'extends': 'cd-base'` to your .eslintrc file.

## Maintenance

This package is maintained by the wizards at [Customer Direct, LLC](http://customerdirect.com) in St. Louis, MO.
