module.exports = {
  rules: {
    'array-bracket-spacing': 0,

    // Follow 1tbs
    'brace-style': [1, '1tbs', { 'allowSingleLine': true }],

    // Require Camelcase
    'camelcase': [1, { 'properties': 'never' }],

    // Allow trailing commas
    'comma-dangle': 0,

    // Enforces spacing around commas
    'comma-spacing': [1, { 'before': false, 'after': true }],

    // Comma style
    'comma-style': [1, 'last'],

    // Enforce consistent linebreak style
    'linebreak-style': [1, 'unix'],

    // Enforce consistent spacing between keys and values
    'key-spacing': [1, { 'beforeColon': false }],

    // Enforce consistent spacing before and after keywords
    'keyword-spacing': [1, { 'before': true }],

    // Enforce consistent indentation
    'indent': [1, 2, { 'SwitchCase': 1 }],

    // Enforce a maximum depth that blocks can be nested
    'max-depth': 1,

    // Enforce a maximum line length
    'max-len': [1, 100, { 'ignoreComments': true }],

    // Enforce a maximum number of statements allowed in function blocks
    'max-statements': [1, { 'max': 18 }],

    // Enforce quote style
    'quotes': [1, 'single', {
      'avoidEscape': true,
      'allowTemplateLiterals': true
    }],

    'semi': [1, 'never']
  }
}
