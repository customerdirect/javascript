module.exports = {
  rules: {
    // Disallow the use of console
    'no-console': 2,

    // Disallow debugger
    'no-debugger': 1,

    // Disallow duplicate arguments in functions
    'no-dupe-args': 1,

    // Disallow a duplicate case label.
    'no-duplicate-case': 1,

    // Disallow empty statements
    'no-empty': 1,

    // Disallow the use of empty character classes in regular expressions
    'no-empty-character-class': 1,

    // Disallow assigning to the exception in a catch block
    'no-ex-assign': 1,

    // Disallow double-negation boolean casts in a boolean context
    'no-extra-boolean-cast': 0,

    // disallow unnecessary parentheses
    // http://eslint.org/docs/rules/no-extra-parens
    'no-extra-parens': [0, 'all', {
      conditionalAssign: true,
      nestedBinaryExpressions: false,
      returnAssign: false,
    }],

    // Disallow unnecessary semicolons
    'no-extra-semi': 1,

    // Disallow overwriting functions written as function declarations
    'no-func-assign': 1,

    // Disallow function or variable declarations in nested blocks
    'no-inner-declarations': 1,

    // Disallow invalid regular expression strings in the RegExp constructor
    'no-invalid-regexp': 1,

    // Disallow irregular whitespace outside of strings and comments
    'no-irregular-whitespace': 1,

    // Disallow negation of the left operand of an in expression
    'no-negated-in-lhs': 1,

    // Disallow the use of object properties of the global object (Math and JSON) as functions
    'no-obj-calls': 1,

    // Disallow multiple spaces in a regular expression literal
    'no-regex-spaces': 1,

    // Disallow sparse arrays
    'no-sparse-arrays': 1,

    // Avoid code that looks like two expressions but is actually one
    'no-unexpected-multiline': 0,

    // Disallow unreachable statements after a return, throw, continue, or break statement
    'no-unreachable': 1,


    // Disallow return/throw/break/continue inside finally blocks
    // http://eslint.org/docs/rules/no-unsafe-finally
    'no-unsafe-finally': 1,

    // Disallow comparisons with the value NaN
    'use-isnan': 1,

    // Ensure that the results of typeof are compared against a valid string
    'valid-typeof': 1
  }
}
