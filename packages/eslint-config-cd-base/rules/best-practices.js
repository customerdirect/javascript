module.exports = {
  rules: {

    // Disallow a setter to be defined without a paired getter.
    'accessor-pairs': 1,

    // Disallow variables to be used outside of the block in which they were defined.
    'block-scoped-var': 1,

    // Limit cyclomatic complexity to 4 paths
    'complexity': [1, 6],

    // Enforce consisten returns.
    'consistent-return': 2,

    // Always require curly braces.
    'curly': 1,

    // Require default case in switch statements
    'default-case': [1, { commentPattern: '^no default$' }],

    // Enforce using dot notation over quare-bracket notiation
    'dot-notation': 1,

    // Require the dot to be on the same line as the property
    'dot-location': [1, 'property'],

    // Always require '===' over '=='
    'eqeqeq': 1,

    // Require an if statement in for-in loops
    'guard-for-in': 1,

    // Disallow `alert`, `confirm`, or `prompt`. Don't be lazy
    'no-alert': 1,

    // Disallow `arguments.caller` or `arguments.callee` to be used, as they are deprecated.
    'no-caller': 2,

    // Always require regex literals to escape division operators
    'no-div-regex': 1,

    // Prevent unnecessary `else` blocks when an `if` contains a `return` statement
    'no-else-return': 1,

    // Disallow `null` comparisons
    'no-eq-null': 1,

    // Never all the use of `eval`
    'no-eval': 1,

    // Disallow extending native prototypes
    'no-extend-native': 1,

    // Prevent function binding when a function does not use `this`
    'no-extra-bind': 1,

    // Prevent unintentional fall throughs in `switch` statements
    'no-fallthrough': 1,

    // Prevent floating decimals; e.g. `2.`
    'no-floating-decimal': 1,

    // Allow implicit type coercion.
    'no-implicit-coercion': 0,

    // Disallow implied use of `eval`
    'no-implied-eval': 1,

    // Never all the use of `this` outside of classes
    'no-invalid-this': 1,

    // Disallow the use of deprecated `__iterator__` property
    'no-iterator': 1,

    // Never all the user of `labels`
    'no-labels': 1,

    // Disallow standalone code blocks delimited by curly braces
    'no-lone-blocks': 1,

    // Disallow functions to be created within a loop
    'no-loop-func': 1,

    // Disallow multiple whitespace characters in expressions
    'no-multi-spaces': 1,

    // Disallow using a `\` chracter to create multi-line strings
    'no-multi-str': 1,

    // Disallow built-in native objects to be reassigned
    'no-native-reassign': 2,

    // Disallow using the `Function` constructor to create functions
    'no-new-func': 1,

    // Disallow using `String`, `Number`, and `Boolean in place of primitives
    'no-new-wrappers': 1,

    // Never use the `new` operator without assignment
    'no-new': 1,

    // Disallow the use of the new operator for Function object
    'no-new-func': 1,

    // Disallow using `String`, `Number`, and `Boolean` in place of primitives
    'no-new-wrappers': 1,

    // Disallow octal escape sequences, which are deprecated.
    'no-octal-escape': 2,

    // Disallow octal literals that begin with a leading zero
    'no-octal': 1,

    // Allow parameter reassignment
    'no-param-reassign': 0,

    // Warn when using `process.env` to prevent it being littered throughout a code base
    'no-process-env': 1,

    // Disallow using deprecated `__proto__` property.
    'no-proto': 1,

    // Disallow a variable to be declared multiple times within the same scope
    'no-redeclare': 1,

    // Disallow assignment in `return` statements.
    'no-return-assign': [1, 'always'],

    // Disallow using `javascript:` in urls.
    'no-script-url': 1,

    // Disallow comparisons where both sides are exactly the same
    'no-self-compare': 1,

    // Disallow using a comma operator to separate multiple expressions
    'no-sequences': 1,

    // Restrict what can be thrown as an exception
    'no-throw-literal': 1,

    // Disallow unmodified conditions of loops
    'no-unmodified-loop-condition': 1,

    // Disallow unused expressions
    'no-unused-expressions': 1,

    // Disallow unused labels
    'no-unused-labels': 1,

    // Disallow using `call` or `apply`
    'no-useless-call': 1,

    // Disallow concatenation of two string literals
    'no-useless-concat': 1,

    // Disallow unnecessary string escaping
    'no-useless-escape': 1,

    // Allow using the `void` operator
    'no-void': 0,

    // Warn when source contains warning comments
    'no-warning-comments': [0, { terms: ['todo', 'fixme', 'xxx'], location: 'start' }],

    // Disallow using the `with` statement
    'no-with': 1,

    // Always require a `radix` parameter to `parseInt()`
    'radix': 1,

    // Always declare variables at the top of their scope to represent hoisting
    'vars-on-top': 0,

    // Always require an immediately invoked function expression (IIFE) to be wrapped
    'wrap-iife': [1, 'outside'],

    // Require that literals come after variables in conditions
    'yoda': 2

  }
}
