module.exports = {
  env: {
    es6: true
  },

  parser: 'babel-eslint',

  parserOptions: {
    ecmaVersion: 7,
    sourceType: 'module',
    ecmaFeatures: {
      generators: false,
      objectLiteralDuplicateProperties: false
    }
  },

  rules: {

    // Use arrow function brackets when yo' want
    'arrow-body-style': 0,

    // Require parens in arrow function arguments
    'arrow-parens': [1, 'always'],

    // Require space before/after arrow function’s arrow
    'arrow-spacing': [1, { 'before': true, 'after': true }],

  }
}
